﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using fasm2.Models;

namespace fasm2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChiNhanhsController : ControllerBase
    {
        private readonly ChiNhanhContext _context;

        public ChiNhanhsController(ChiNhanhContext context)
        {
            _context = context;
        }

        // GET: api/ChiNhanhs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ChiNhanh>>> GetChiNhanhItems()
        {
          if (_context.ChiNhanhItems == null)
          {
              return NotFound();
          }
            return await _context.ChiNhanhItems.ToListAsync();
        }

        // GET: api/ChiNhanhs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ChiNhanh>> GetChiNhanh(int id)
        {
          if (_context.ChiNhanhItems == null)
          {
              return NotFound();
          }
            var chiNhanh = await _context.ChiNhanhItems.FindAsync(id);

            if (chiNhanh == null)
            {
                return NotFound();
            }

            return chiNhanh;
        }

        // PUT: api/ChiNhanhs/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutChiNhanh(int id, ChiNhanh chiNhanh)
        {
            if (id != chiNhanh.BranchId)
            {
                return BadRequest();
            }

            _context.Entry(chiNhanh).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChiNhanhExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ChiNhanhs
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ChiNhanh>> PostChiNhanh(ChiNhanh chiNhanh)
        {
          if (_context.ChiNhanhItems == null)
          {
              return Problem("Entity set 'ChiNhanhContext.ChiNhanhItems'  is null.");
          }
            _context.ChiNhanhItems.Add(chiNhanh);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetChiNhanh", new { id = chiNhanh.BranchId }, chiNhanh);
        }

        // DELETE: api/ChiNhanhs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteChiNhanh(int id)
        {
            if (_context.ChiNhanhItems == null)
            {
                return NotFound();
            }
            var chiNhanh = await _context.ChiNhanhItems.FindAsync(id);
            if (chiNhanh == null)
            {
                return NotFound();
            }

            _context.ChiNhanhItems.Remove(chiNhanh);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ChiNhanhExists(int id)
        {
            return (_context.ChiNhanhItems?.Any(e => e.BranchId == id)).GetValueOrDefault();
        }
    }
}
