﻿using Microsoft.EntityFrameworkCore;
namespace fasm2.Models
{
    public class ChiNhanhContext : DbContext
    {
        public ChiNhanhContext(DbContextOptions<ChiNhanhContext> options)
        : base(options)
        {

        }

        public DbSet<ChiNhanh> ChiNhanhItems { get; set; } = null!;
    }
}
